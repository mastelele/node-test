module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('Accounts', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
        type: Sequelize.UUID,
      },
      balance: {
        allowNull: false,
        defaultValue: 0,
        type: Sequelize.NUMERIC.UNSIGNED,
      },
      reserved: {
        allowNull: false,
        defaultValue: 0,
        type: Sequelize.NUMERIC.UNSIGNED,
      },
      currency: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }),
  down: queryInterface => queryInterface.dropTable('Accounts'),
}
