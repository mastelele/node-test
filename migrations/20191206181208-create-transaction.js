module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('Transactions', {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
        type: Sequelize.UUID,
      },
      type: {
        allowNull: false,
        defaultValue: '',
        type: Sequelize.STRING,
      },
      amount: {
        allowNull: false,
        defaultValue: 0,
        type: Sequelize.NUMERIC.UNSIGNED,
      },
      isCompleteOrCanceled: {
        allowNull: true,
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }),
  down: queryInterface => queryInterface.dropTable('Transactions'),
}
