module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface
      .addColumn('Accounts', 'UserId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      })
      .then(() =>
        queryInterface.addColumn('Transactions', 'AccountId', {
          type: Sequelize.UUID,
          references: {
            model: 'Accounts',
            key: 'id',
          },
          onUpdate: 'CASCADE',
          onDelete: 'CASCADE',
        }),
      ),
  down: queryInterface =>
    queryInterface
      .removeColumn('Accounts', 'UserId')
      .then(() => queryInterface.removeColumn('Transactions', 'AccountId')),
}
