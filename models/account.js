module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define('Account', {
    id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      type: DataTypes.UUID,
    },
    balance: {
      type: DataTypes.NUMERIC.UNSIGNED,
      defaultValue: 0,
      allowNull: false,
    },
    reserved: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.NUMERIC.UNSIGNED,
    },
    currency: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  })
  Account.associate = models => {
    models.Account.hasMany(models.Transaction, {
      as: 'transactions',
    })
  }
  return Account
}
