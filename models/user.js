module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
  })
  User.associate = models => {
    models.User.hasMany(models.Account, { as: 'accounts' })
  }
  return User
}
