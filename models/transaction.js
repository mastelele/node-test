module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    id: {
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
      type: DataTypes.UUID,
    },
    type: {
      allowNull: false,
      defaultValue: '',
      type: DataTypes.STRING,
    },
    amount: {
      allowNull: false,
      defaultValue: 0,
      type: DataTypes.NUMERIC.UNSIGNED,
    },
    isCompleteOrCanceled: {
      allowNull: true,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
  })
  return Transaction
}
