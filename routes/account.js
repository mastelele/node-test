const router = require('koa-router')()

const transactions = require('./transaction')
const {
  account: {
    createAccount,
    getAccounts,
    getAccount,
    deleteAccount,
    getTransactions,
  },
} = require('../middlewares')

router.prefix('/account')

router.get('/', getAccounts)

router.get('/:accountId/transaction', getTransactions)

router.get('/:accountId', getAccount)

router.post('/', createAccount)

router.del('/:accountId', deleteAccount)

router.use('/transaction', transactions.routes(), transactions.allowedMethods())

module.exports = router
