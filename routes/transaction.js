const router = require('koa-router')()

const {
  transaction: { makeTransaction },
} = require('../middlewares')

router.post('/', makeTransaction)

module.exports = router
