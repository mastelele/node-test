const router = require('koa-router')()

const accounts = require('./account')

const {
  user: { createUser, getUser },
} = require('../middlewares')

router.prefix('/user')

router.get('/:id', getUser)

router.post('/', createUser)

router.use('/:userId', accounts.routes(), accounts.allowedMethods())

module.exports = router
