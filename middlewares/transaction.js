const { Transaction, Account } = require('../models')

const TransactionTypes = {
  DEPOSIT: 'deposit',
  WITHDRAW: 'withdraw',
  RESERVATION: 'reserve',
  CONFIRM_RESERVATION: 'confirm-reserve',
  CANCEL_RESERVATION: 'cancel-reserve',
}

const handleDeposit = async (userId, id, amount, type) => {
  try {
    await Account.increment('balance', {
      by: amount,
      where: {
        UserId: userId,
        id,
      },
    })
    await Transaction.create({
      type,
      amount,
      AccountId: id,
    })
    return id
  } catch (err) {
    throw err
  }
}

const handleWithDraw = async (userId, id, amount, type) => {
  try {
    const account = await Account.findOne({
      where: {
        id,
        UserId: userId,
      },
    })
    if (!account) return Promise.reject(new Error('Account not exist'))
    const { balance, reserved } = account
    if (balance - reserved >= amount) {
      await Account.decrement('balance', {
        by: amount,
        where: {
          UserId: userId,
          id,
        },
      })
      await Transaction.create({
        type,
        amount,
        AccountId: id,
      })
      return id
    }
    return Promise.reject(new Error('balance not enough'))
  } catch (err) {
    throw err
  }
}

const handleReserevtation = async (userId, id, amount, type) => {
  try {
    const account = await Account.findOne({
      where: {
        id,
        UserId: userId,
      },
    })
    if (!account) return Promise.reject(new Error('Account not exist'))
    const { balance, reserved } = account
    if (balance - reserved >= amount) {
      await Account.increment('reserved', {
        by: amount,
        where: {
          UserId: userId,
          id,
        },
      })
      await Transaction.create({
        type,
        amount,
        AccountId: id,
      })
      return id
    }
    return Promise.reject(new Error('balance not enough'))
  } catch (err) {
    throw err
  }
}

const handleConfirmReservation = async (userId, transactionId, type) => {
  try {
    const transaction = await Transaction.findOne({
      where: {
        id: transactionId,
        type: TransactionTypes.RESERVATION,
        isCompleteOrCanceled: false,
      },
    })
    if (!transaction) return Promise.reject(new Error('Transaction not exist'))
    const { amount, AccountId } = transaction
    await Account.decrement(['balance', 'reserved'], {
      by: amount,
      where: {
        UserId: userId,
        id: AccountId,
      },
    })
    await transaction.update({ isCompleteOrCanceled: true })
    await Transaction.create({
      type,
      amount,
      AccountId,
    })
    return AccountId
  } catch (err) {
    throw err
  }
}

const handleCancelReservation = async (userId, transactionId, type) => {
  try {
    const transaction = await Transaction.findOne({
      where: {
        id: transactionId,
        type: TransactionTypes.RESERVATION,
        isCompleteOrCanceled: false,
      },
    })
    if (!transaction) return Promise.reject(new Error('Transaction not exist'))
    const { amount, AccountId } = transaction
    await Account.decrement('reserved', {
      by: amount,
      where: {
        UserId: userId,
        id: AccountId,
      },
    })
    await transaction.update({ isCompleteOrCanceled: true })
    await Transaction.create({
      type,
      amount,
      AccountId,
    })
    return AccountId
  } catch (err) {
    throw err
  }
}

const makeTransaction = async ctx => {
  const { id, type, amount } = ctx.request.body
  const { userId } = ctx.params
  let makeOperation = null
  try {
    switch (type) {
      case TransactionTypes.DEPOSIT:
        makeOperation = handleDeposit(userId, id, amount, type)
        break
      case TransactionTypes.WITHDRAW:
        makeOperation = handleWithDraw(userId, id, amount, type)
        break
      case TransactionTypes.RESERVATION:
        makeOperation = handleReserevtation(userId, id, amount, type)
        break
      case TransactionTypes.CONFIRM_RESERVATION:
        makeOperation = handleConfirmReservation(userId, id, type)
        break
      case TransactionTypes.CANCEL_RESERVATION:
        makeOperation = handleCancelReservation(userId, id, type)
        break
      default:
        throw new Error('Incorrect transaction type')
    }
    const accountId = await makeOperation
    const account = await Account.findOne({
      where: {
        UserId: userId,
        id: accountId,
      },
    })
    ctx.body = account
  } catch (err) {
    ctx.throw(400, err.message)
  }
}

module.exports = {
  makeTransaction,
}
