const { Account, Transaction } = require('../models')

const getAccounts = async ctx => {
  const { userId } = ctx.params
  try {
    const accounts = await Account.findAll({
      where: {
        UserId: Number(userId),
      },
    })
    ctx.body = accounts
  } catch (err) {
    ctx.throw(400, err.message)
  }
}

const getAccount = async ctx => {
  try {
    const { accountId, userId } = ctx.params
    const account = await Account.findOne({
      where: {
        UserId: Number(userId),
        id: accountId,
      },
    })
    ctx.body = account
  } catch (err) {
    ctx.throw(400, err.message)
  }
}

const createAccount = async ctx => {
  const { userId } = ctx.params
  const { currency } = ctx.request.body
  try {
    const account = await Account.create(
      {
        UserId: Number(userId),
        currency,
      },
      {
        returning: true,
      },
    )
    ctx.body = account
  } catch (err) {
    ctx.throw(400, err.message)
  }
}

const deleteAccount = async ctx => {
  const { userId, accountId } = ctx.params
  try {
    const result = await Account.destroy({
      where: {
        UserId: userId,
        id: accountId,
      },
    })
    if (result) {
      ctx.body = ''
    } else {
      ctx.throw(400, 'Account or user not exist')
    }
  } catch (err) {
    ctx.throw(400, err.message)
  }
}

const getTransactions = async ctx => {
  const { userId, accountId } = ctx.params
  try {
    const account = await Account.findOne({
      where: {
        UserId: Number(userId),
        id: accountId,
      },
      include: [{ model: Transaction, as: 'transactions' }],
    })
    if (!account) {
      ctx.throw(400, 'Account not exist')
    } else {
      const { transactions } = account
      ctx.body = transactions
    }
  } catch (err) {
    ctx.throw(400, err.message)
  }
}

module.exports = {
  createAccount,
  getAccounts,
  getAccount,
  deleteAccount,
  getTransactions,
}
