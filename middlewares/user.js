const { User } = require('../models')

const getUser = async ctx => {
  const { id } = ctx.params
  const user = await User.findOne(
    {
      where: {
        id,
      },
    },
    {
      returning: true,
    },
  )
  ctx.body = JSON.stringify(user)
}

const createUser = async ctx => {
  const user = await User.create(
    {},
    {
      returning: true,
    },
  )
  ctx.body = JSON.stringify(user)
}

module.exports = {
  getUser,
  createUser,
}
